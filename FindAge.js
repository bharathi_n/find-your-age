import React from 'react';

export default class FindAge extends React.Component {
  constructor(props){
    super(props);
    this.state={
      dob:new Date()
    }
    this.handleChange=this.handleChange.bind(this);
  }
handleChange(){
  var date2 = new Date();
  var date1 = new Date(document.getElementById("dob").value);
  const diffTime=Math.abs(date2.getTime()-date1.getTime());
  const diffDays= Math.ceil(diffTime/(1000*60*60*24));
  alert("your age is:" + Math.floor(diffDays/365.25));
}
render(){
  return(
    <div>
      <input type= "date" id="dob"></input>
      <input type="submit" onClick={this.handleChange}></input>
    </div>
  );
}
}
